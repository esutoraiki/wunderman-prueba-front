"use strict";

const gulp = require("gulp"),
    del = require("del"),
    sass = require("gulp-sass"),
    svgmin = require("gulp-svgmin"),
    postcss = require("gulp-postcss");

gulp.task("delete_svg", function () {
    return del("src/assets/images/svg/*.svg");
});

gulp.task("svgmin", function () {
    return gulp.src("src/assets/images/svg/orig/*")
        .pipe(svgmin(
            { removeStyleElement: true },
            { removeComments: true }
        ))
        .pipe(gulp.dest("src/assets/images/svg/"));
})

gulp.task("process_svg", function () {
    return gulp.src("src/assets/css/svg.css")
        .pipe(postcss([
            require("postcss-inline-svg")({
                removeFill: true
            })
        ]))
        .pipe(gulp.dest("src/assets/css/"));
})

gulp.task("css_svg", function () {
    return gulp.src("src/assets/sass/svg.scss")
        .pipe(sass({
            outputStyle: "compressed"
        }).on("error", sass.logError))
        .pipe(gulp.dest("src/assets/css/"));
});

gulp.task("watch", function () {
    console.log("");
    console.log("---- INICIANDO WATCH ----");

    gulp.watch("src/assets/sass/svg.scss", gulp.series("css_svg", "process_svg"));
    gulp.watch("src/assets/images/svg/orig/*.svg", gulp.series(
        "delete_svg",
        "svgmin",
        "css_svg",
        "process_svg"
    ));
});

gulp.task("default", gulp.series("watch"));
