import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule} from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HomeComponent } from "@modules/pages/home/home.component";
import { NotfoundComponent } from "@modules/pages/notfound/notfound.component";
import { AboutusComponent } from "@modules/pages/aboutus/aboutus.component";
import { ContactComponent } from "@modules/pages/contact/contact.component";

import { HeaderComponent } from "@components/header/header.component";
import { MenuComponent } from "@components/menu/menu.component";
import { FooterComponent } from "@components/footer/footer.component";
import { FormContactComponent } from "@components/form-contact/form-contact.component";

import { UsersService } from "@services/users.service";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotfoundComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    ContactComponent,
    FormContactComponent,
    AboutusComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
