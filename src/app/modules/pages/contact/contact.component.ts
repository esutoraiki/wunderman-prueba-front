import { Component, OnInit } from "@angular/core";
import { FormContactComponent } from "@components/form-contact/form-contact.component";

const CLASSBODY = "contact_page";

@Component({
  selector: "contact-component",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"]
})
export class ContactComponent implements OnInit {
    ngOnInit() { document.body.classList.add(CLASSBODY); }
}
