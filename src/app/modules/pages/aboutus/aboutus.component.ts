import { Component, OnInit } from "@angular/core";
import { UsersService } from "@services/users.service";

const CLASSBODY = "about_page";

@Component({
  selector: "app-aboutus",
  templateUrl: "./aboutus.component.html",
  styleUrls: ["./aboutus.component.scss"]
})

export class AboutusComponent implements OnInit {
    public users:any[] = [];

    constructor (protected usersService: UsersService) {}

    ngOnInit() {
        document.body.classList.add(CLASSBODY);
        this.usersService.getUser()
            .subscribe((data) => {
                this.users = data["data"];
            },
            (error) => {
                console.error(error);
            });
    }
}
