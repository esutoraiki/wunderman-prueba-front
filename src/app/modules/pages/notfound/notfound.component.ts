import { Component, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import Utils from "@shared/helper/utils";

const CLASSBODY = "nofound_page";

@Component({
  templateUrl: "./notfound.component.html",
  styleUrls: ["./notfound.component.scss"]
})

export class NotfoundComponent implements AfterViewInit {
    @ViewChild("errorVideo") videoplayer: ElementRef;

    ngAfterViewInit () {
        document.body.classList.add(CLASSBODY);

        let video:HTMLVideoElement = this.videoplayer.nativeElement;

        video.muted = true;
        video.play();
    }

    clickLink() {
        Utils.removeClass();
    }
}
