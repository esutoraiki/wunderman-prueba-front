import { Component, OnInit } from "@angular/core";

const CLASSBODY = "home_page";

@Component({
  selector: "home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})

export class HomeComponent implements OnInit {
    ngOnInit() { document.body.classList.add(CLASSBODY); }
}
