import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "@modules/pages/home/home.component";
import { NotfoundComponent } from "@modules/pages/notfound/notfound.component";
import { ContactComponent } from "@modules/pages/contact/contact.component";
import { AboutusComponent } from "@modules/pages/aboutus/aboutus.component";

const routes: Routes = [
    { path: "", component: HomeComponent},
    { path: "contact", component: ContactComponent},
    { path: "about", component: AboutusComponent },
    { path: "**", component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
