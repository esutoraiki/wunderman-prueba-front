import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

export interface InterfaceUsers {
    avatar:string;
    email:string;
    first_name:string;
    last_name:string;
}

@Injectable({
  providedIn: "root"
})

export class UsersService {
  constructor(protected http:HttpClient) { }

  getUser():Observable<InterfaceUsers> {
      return this.http.get<InterfaceUsers>("https://reqres.in/api/users?page=1&per_page=10");
  }
}
