import { Component } from "@angular/core";
import { MenuComponent } from "../menu/menu.component";

@Component({
  selector: "header-component",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})

export class HeaderComponent {
  constructor() { }
}
