import { Component } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "form-contact-component",
  templateUrl: "./form-contact.component.html",
  styleUrls: ["./form-contact.component.scss"]
})
export class FormContactComponent {
    public submitted:boolean = false;
    public formcontact = new FormGroup({
        name: new FormControl("", Validators.required),
        email: new FormControl("", Validators.required)
    });

    public contact() {
        this.submitted = true;
    }
}
