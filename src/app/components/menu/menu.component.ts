import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import Utils from "@shared/helper/utils";

const CLASSBODY = "no_scroll";

@Component({
  selector: "menu-component",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})

export class MenuComponent implements OnInit {
    public menuOpen:boolean = false;
    public isHome:boolean = false;
    public isContact:boolean = false;
    public isAbout:boolean = false;

    constructor( private router: Router) {}

    ngOnInit () {
        this.isHome = false;
        this.isContact = false;
        this.isAbout = false;

        switch(this.router.url) {
            case "/":
                this.isHome = true;
            break;
            case "/contact":
                this.isContact = true;
            break;
            case "/about":
                this.isAbout = true;
            break;
        }
    }

    public actionMenu() {
        this.menuOpen = !this.menuOpen;

        if (this.menuOpen) {
            document.body.classList.add(CLASSBODY);
        } else {
            document.body.classList.remove(CLASSBODY);
        }
    }

    public actionItemMenu() {
        Utils.removeClass();
    }
}
